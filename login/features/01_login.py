from selenium import webdriver
import lettuce_webdriver.webdriver
from lettuce import *
import time
import settings

@before.all
def setup_browser():
    world.browser = webdriver.Firefox()

@step('I am on siminars.com/continue')
def on_siminars(step):
    world.browser.get(settings.URl_STAGGING+"/continue") 
    time.sleep(4)
    if(world.browser.current_url != settings.URl_STAGGING+"/continue"):
        world.browser.get(settings.URl_STAGGING+"/logout.sv") 


@step('I add email and password')
def add_email_and_password(step):
    email = world.browser.find_element_by_name("email")
    email.send_keys(settings.USER_NAME)
    password = world.browser.find_element_by_name("password")
    password.send_keys(settings.PASSWORD)
    signUp = world.browser.find_element_by_class_name("signupButton")
    signUp.click()

@step('I should be logged in')
def should_be_logged_in(step):
	time.sleep(4)
	assert (world.browser.current_url == "http://siminars.com/profile")

@step('I click on "Continue with google" button')
def continue_with_google(step):
    time.sleep(2)
    world.browser.get("https://accounts.google.com/ServiceLogin?service=mail&continue=https://mail.google.com/mail/")
    time.sleep(2)
    email = world.browser.find_element_by_id("Email")
    email.send_keys("email@gmail.com")
    next_btn= world.browser.find_element_by_id("next")
    next_btn.click()
    time.sleep(2)
    Passwd = world.browser.find_element_by_id("Passwd")
    Passwd.send_keys("password")
    signIn = world.browser.find_element_by_id("signIn")
    signIn.click()
    world.browser.get("http://siminars.com/continue") 
    time.sleep(4)
    google = world.browser.find_element_by_class_name("smi-google-plus-circle")
    google.click()

@step( 'I click on "Continue with Facebook" button')
def continue_with_fb(step):
    time.sleep(2)
    world.browser.get("https://www.facebook.com/?_rdr")
    time.sleep(2)
    email = world.browser.find_element_by_id("email")
    email.send_keys("email@gmail.com")
    Passwd = world.browser.find_element_by_id("pass")
    Passwd.send_keys("password")
    signIn = world.browser.find_element_by_id("u_0_v")
    signIn.click()
    world.browser.get("http://siminars.com/continue") 
    time.sleep(4)
    fb = world.browser.find_element_by_class_name("smi-facebook-circle")
    fb.click()