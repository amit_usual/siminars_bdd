Feature: Login to siminars
  As a siminars user
  I should be able to login

  Scenario: Login user with email
    Given I am on siminars.com/continue
    When I add email and password
    Then I should be logged in

  Scenario: Login user with gmail
  	Given I am on siminars.com/continue
  	I click on "Continue with google" button
  	Then I should be logged in

  Scenario: Login user with facebook
    Given I am on siminars.com/continue
    I click on "Continue with Facebook" button
    Then I should be logged in