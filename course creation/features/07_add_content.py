from selenium import webdriver
import lettuce_webdriver.webdriver
from selenium.webdriver.common.keys import Keys
from lettuce import *
import time
import sys
sys.path.append('/home/aayush/work/siminars_bdd')
import settings


@step('Given I am on step')
def on_step(step):
    current_url = world.browser.current_url
    assert ("d?sv=e" in current_url and '18098' in current_url)


@step('When I select action to-do on add lesson')
def select_action(step):
    time.sleep(3)
    add_lesson = world.browser.find_element_by_link_text("Add Lesson")
    add_lesson.click()
    time.sleep(1)
    select_action = world.browser.find_element_by_link_text("Action To-Do")
    select_action.click()


@step('And I add description')
def and_i_add_description(step):
    time.sleep(4)
    description = world.browser.find_element_by_name("title")
    description.send_keys(Keys.ENTER)
    description.send_keys("test Action")
    publish = world.browser.find_element_by_class_name("publish")
    publish.click()


@step('Then action to-do should be create')
def action_create(step):
    time.sleep(4)
    check_text = world.browser.find_element_by_class_name("svpostheader")
    assert('test Action' in check_text.text)


@step('When I select audio on add lesson')
def select_audio(step):
    time.sleep(2)
    add_lesson = world.browser.find_element_by_link_text("Add Lesson")
    add_lesson.click()
    time.sleep(1)
    select_action = world.browser.find_element_by_link_text("Audio")
    select_action.click()


@step('And I added audio link')
def and_i_add_aduio(step):
    time.sleep(2)
    val = world.browser.find_element_by_name("val")
    val.send_keys(
        "https://soundcloud.com/umayrsajid/na-jaane-kya-hai-tumse-waasta-kuch-kuch-locha-hai")
    publish = world.browser.find_element_by_class_name("next")
    publish.click()
    time.sleep(2)
    publish = world.browser.find_element_by_class_name("publish")
    publish.click()


@step('Then audio should be added')
def audio_added(step):
    time.sleep(4)
    check_text = world.browser.find_element_by_class_name("svpostheader")
    assert('Na Jaane Kya Hai Tumse' in check_text.text)


@step('When I select blog on add lesson')
def select_blog(step):
    time.sleep(2)
    add_lesson = world.browser.find_element_by_link_text("Add Lesson")
    add_lesson.click()
    time.sleep(1)
    select_action = world.browser.find_element_by_link_text("Blog Post")
    select_action.click()


@step('And I added title, body and save')
def add_body(step):
    time.sleep(3)
    title = world.browser.find_element_by_name("title")
    title.send_keys('test blog')
    title.send_keys(Keys.TAB)
    time.sleep(2)
    blog_body = world.browser.find_element_by_class_name("sveditblob")
    blog_body.send_keys('test body')
    save_post = world.browser.find_element_by_link_text("Save Post")
    save_post.click()


@step('Then blog is created')
def blog_created(step):
    time.sleep(3)
    check_text = world.browser.find_element_by_class_name("svpostbody")
    assert('test body' in check_text.text)


@step('when I slect Book Reading on add lesson')
def select_book(step):
    time.sleep(2)
    add_lesson = world.browser.find_element_by_link_text("Add Lesson")
    add_lesson.click()
    time.sleep(1)
    select_action = world.browser.find_element_by_link_text("Book Reading")
    select_action.click()


@step('And I search book and add it')
def book_search(step):
    time.sleep(3)
    title = world.browser.find_element_by_name("search")
    title.send_keys('Technical Manual')
    time.sleep(10)
    add_book = world.browser.find_element_by_class_name("btn-small")
    add_book.click()
    publish = world.browser.find_element_by_class_name("publish")
    publish.click()

@step('Then book read is created')
def book_added(step):
    time.sleep(3)
    check_text = world.browser.find_element_by_class_name("svpostheader")
    assert('Technical Manual' in check_text.text)

@step('When I select slide on add lesson')
def select_slide(step):
    time.sleep(2)
    add_lesson = world.browser.find_element_by_link_text("Add Lesson")
    add_lesson.click()
    time.sleep(1)
    select_action = world.browser.find_element_by_link_text("Slide Show")
    select_action.click()

@step('And I added slide link')
def and_i_add_slide(step):
    time.sleep(6)
    # import pdb;pdb.set_trace()
    val = world.browser.find_element_by_name("val")
    val.send_keys(
        "http://www.slideshare.net/nowells/introduction-to-python-5182313")
    publish = world.browser.find_element_by_class_name("next")
    publish.click()
    time.sleep(2)
    publish = world.browser.find_element_by_class_name("publish")
    publish.click()


@step('Then slide should be added')
def slide_added(step):
    time.sleep(3)
    check_text = world.browser.find_element_by_class_name("svpostheader")
    assert('Python' in check_text.text)

@step('I create other step')
def create_step(step):
    add_step = world.browser.find_element_by_class_name('smaddstep') 
    add_step.click()
    time.sleep(2)
    title = world.browser.find_element_by_name('title')
    title.send_keys('test step')
    save = world.browser.find_element_by_class_name('on_true')
    save.click()

@step('When I select video on add lesson')
def select_video(step):
    time.sleep(2)
    add_lesson = world.browser.find_element_by_link_text("Add Lesson")
    add_lesson.click()
    time.sleep(1)
    select_action = world.browser.find_element_by_link_text("Video")
    select_action.click()


@step('And I added video link')
def and_i_add_video(step):
    time.sleep(3)
    val = world.browser.find_element_by_name("val")
    val.send_keys("https://www.youtube.com/watch?v=TBt1f5XxOtw")
    publish = world.browser.find_element_by_class_name("next")
    publish.click()
    time.sleep(2)
    publish = world.browser.find_element_by_class_name("publish")
    publish.click()


@step('Then video should be added')
def video_added(step):
    time.sleep(3)
    check_text = world.browser.find_element_by_class_name("svpostheader")
    assert('Namak Haraam' in check_text.text)

@step('When I select Discussion Question on add lesson')
def select_question(step):
    time.sleep(2)
    add_lesson = world.browser.find_element_by_link_text("Add Lesson")
    add_lesson.click()
    time.sleep(1)
    select_action = world.browser.find_element_by_link_text("Discussion Question")
    select_action.click()   

@step('And I added Discussion Question and save it')
def add_question(step):
    time.sleep(2)
    set_question = world.browser.find_element_by_name("title")
    set_question.send_keys(Keys.ENTER)
    set_question.send_keys("test Question")
    publish = world.browser.find_element_by_class_name("publish")
    publish.click()

@step('Then Discussion Question should be created')
def added_question(step):
    time.sleep(3)
    check_text = world.browser.find_element_by_class_name("svpostheader")
    assert('test Question' in check_text.text)

@step('When I select Poll on add lesson')
def select_poll(step):
    time.sleep(2)
    add_lesson = world.browser.find_element_by_link_text("Add Lesson")
    add_lesson.click()
    time.sleep(1)
    select_action = world.browser.find_element_by_link_text("Poll")
    select_action.click()   

@step('And I added Poll Question, Poll Response Options and save it')
def add_poll(step):
    time.sleep(2)
    set_question = world.browser.find_element_by_name("title")
    set_question.send_keys(Keys.ENTER)
    set_question.send_keys("test poll")
    set_question = world.browser.find_element_by_name("options")
    set_question.send_keys(Keys.ENTER)
    set_question.send_keys("test options")
    publish = world.browser.find_element_by_class_name("publish")
    publish.click()

@step('Then Poll should be created')
def poll_created(step):
    time.sleep(3)
    check_text = world.browser.find_element_by_class_name("svpostheader")
    assert('test poll' in check_text.text)
    

@step('When I select quiz on add lesson')
def select_quiz(step):
    time.sleep(2)
    add_lesson = world.browser.find_element_by_link_text("Add Lesson")
    add_lesson.click()
    time.sleep(1)
    select_quiz = world.browser.find_element_by_link_text("Quiz")
    select_quiz.click() 


@step('I add title and description')
def add_description(step):  
    title = world.browser.find_element_by_name('title')
    title.send_keys("test quiz")
    title.send_keys(Keys.TAB)
    editor1 = world.browser.find_element_by_id('editor1')
    editor1.send_keys("test quiz")


@step('I click save and next')
def save(step): 
    time.sleep(3)   
    save = world.browser.find_element_by_id("save_btn")
    save.click()

@step('I Add question and Answer Options')
def add_question(step):
    time.sleep(3)
    question = world.browser.find_element_by_id("editor4")
    question.click()
    question.send_keys("test question")
    answer =  world.browser.find_element_by_link_text("True or False")
    answer.click()

@step('I click done')
def finished_adding(step):
    time.sleep(3)
    done =  world.browser.find_element_by_link_text("Done")
    done.click()

@step('Publish quiz')
def publish(step):
    time.sleep(3)
    update =  world.browser.find_element_by_class_name("update")
    update.click()


@step('Then quiz should be create')
def check(step):
    time.sleep(3)
    check = world.browser.find_element_by_class_name("svpostbody")
    assert("test quiz" in check.text)