Feature: Manage plan on siminars
  As a siminars user
  I should be able to create bundle and manage

  Scenario: Create bundle
    Given I am on siminars.com on bundle tab
    I click on Add a Bundle or Series
    Then add Bundle Title,choose Type and click create
    Then Bundle is created

  Scenario: Add content to bundle
    Given I am on siminars.com bundle summary page
    I click on Add a courses, selecte course and click add
    Then course is added

  Scenario: Set price of bundle
    I click on Set Price & Publish
    Then set list price, purchase price and click on set bundle price
    Then bundle price set

  Scenario: Discount Coupons
    I click on Coupons and Add a Coupon
    Then set coupon code, price with coupon, expire date and click save
    Then coupon is created
    Then done and next

  Scenario: Signup Options
    Then add Topics
    Then add Promotional Video, Promotional Description
    Then set Sign-up Page Layout

  Scenario: Publish your Bundle
    Given I am on siminars.com bundle launch page
    Then click on launch bundle
    Then bundle lunched