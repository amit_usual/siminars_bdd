from selenium import webdriver
import lettuce_webdriver.webdriver
from lettuce import *
import time
import sys
sys.path.append('/home/aayush/work/siminars_bdd')
import settings


@step('I am on siminars.com/profile')
def i_am_on_siminars_com_profile(step):
	world.browser.get(settings.URl_STAGGING+"/profile") 


@step('When I click on create course')
def create_course(step):
    time.sleep(2)
    world.browser.get(settings.URl_STAGGING+"/profile?siminar_creation=true") 

@step('And I add course title  and tagline')
def add_title_and_tagline(step):
    time.sleep(4)
    title = world.browser.find_element_by_name("title")
    title.send_keys("test course")
    subtitle = world.browser.find_element_by_name("subtitle")
    subtitle.send_keys("123456")
    create = world.browser.find_element_by_class_name("on_true")
    create.click()

@step('I should be course create')
def should_be_course_create(step):
    time.sleep(2)
    current_url = world.browser.current_url
    course_title = world.browser.find_element_by_class_name("smsiminartitle")
    assert ("summary.sv" in current_url and 'test course' in course_title.text)
