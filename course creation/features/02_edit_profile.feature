Feature: Edit my profile on siminars
  As a siminars user
  I should be able to Edit my profile

  Scenario: Edit Name, Bio and Privacy
    I am on siminars.com/profile
    When I click on Edit Name, Bio and Privacy
    Then my basic info edit

  Scenario: Edit email address
    I am on siminars.com/profile
    When I click on email address
    Then my email edit   

  Scenario: Edit password
    I am on siminars.com/profile
    When I click on password
    Then my password edit    

  Scenario: Edit Connect Social Networks
    I am on siminars.com/profile
    When I click on Connect Social Networks
    Then my Connect Social Networks edit   