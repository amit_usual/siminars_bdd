from selenium import webdriver
import lettuce_webdriver.webdriver
from selenium.webdriver.common.keys import Keys
from lettuce import *
import time
import sys
sys.path.append('/home/aayush/work/siminars_bdd')
import settings


@step('Given I am on siminars.com plan page and on Subscription tab')
def goto_subscribe(step):
    world.browser.get(settings.URl_STAGGING+"/profile")
    time.sleep(2)
    dropdown = world.browser.find_element_by_id("down")
    dropdown.click()
    manage_plan = world.browser.find_element_by_link_text('Manage Plans')
    manage_plan.click()
    time.sleep(4)
    new_plan = world.browser.find_element_by_link_text('Manage')
    new_plan.click()
    url_for_Subscription = world.browser.current_url + '/subscribes'
    world.browser.get(url_for_Subscription)


@step('I click on Add a Membership Subscription')
def click_on_subscription(step):
    time.sleep(4)
    subscription = world.browser.find_element_by_class_name(
        "js_add_subscribe_link")
    subscription.click()


@step('Then add Membership Subscription Name and click Create')
def add_title(step):
    time.sleep(4)
    subscription_title = world.browser.find_element_by_name("title")
    subscription_title.send_keys('test subscription')
    create = world.browser.find_element_by_class_name("on_true")
    create.click()


@step('Then Subscription is Created')
def subscription_created(step):
    time.sleep(4)
    assert ("44952" in world.browser.current_url)


@step('given I am on siminars.com subscription summary page')
def on_subscription(step):
    assert ("summary.sv" in world.browser.current_url)


@step('I click on add monthly installment')
def click_on_month(step):
    add_month = world.browser.find_element_by_class_name("js_add_months")
    add_month.click()


@step('Then monthly installment is added')
def month_created(step):
    time.sleep(2)
    check = world.browser.find_element_by_id('replace_content')
    assert(check.text.split()[0] == 'Month')


@step('Given I am on siminars.com subscription summary page')
def on_subscription(step):
    assert ("summary.sv" in world.browser.current_url)


@step('I click on add content')
def click_add_content(step):
    add_content = world.browser.find_element_by_name('Add_Content')
    add_content.click()
    time.sleep(2)


@step('Then selecte course and click add')
def selecte_list(step):
    content_list = world.browser.find_element_by_name('content')
    content_list.click()
    content_list.send_keys(Keys.TAB)
    content_list.click()
    add_btn = world.browser.find_element_by_class_name('on_true')
    add_btn.click()


@step('Then course is addded')
def added_content(step):
    check = world.browser.find_element_by_id('replace_content')
    assert('Siminar' in check.text or 'Bundle' in check.text)


@step('Given I am on subscription summary page')
def on_subscription_summary(step):
    assert ("summary.sv" in world.browser.current_url)


@step('Then I click on publish month')
def publish_month(step):
    time.sleep(2)
    publish = world.browser.find_element_by_name('Publish_Month')
    publish.click()


@step('Then month is published')
def course_published(step):
    pass


@step('I click on price')
def click_price(step):
    current_url = world.browser.current_url
    price_url = current_url.replace('summary.sv?edit=true', 'pricing/price')
    world.browser.get(price_url)
    time.sleep(2)


@step('Then set List Price, Monthly Fee')
def set_price(step):
    #list_price = world.browser.find_element_by_name('list_price')
    #list_price.send_keys('10')
    monthly_fee = world.browser.find_element_by_name('purchase_price')
    monthly_fee.send_keys('10')


@step('Then click on Set Subscription Price')
def click_set(step):
    click_set_price = world.browser.find_element_by_id('js_set_price')
    click_set_price.click()


@step('Then  Subscription price setted')
def price_setted(step):
    time.sleep(4)
    world.browser.get(world.browser.current_url)
    time.sleep(2)
    monthly_fee = world.browser.find_element_by_name('purchase_price')
    assert(monthly_fee.get_attribute('value') == '10.00')
    


@step('Given I am on subscription publish page')
def goto_subscription_publish(step):
    time.sleep(2)
    publish  = world.browser.find_element_by_link_text('Publish')
    publish.click()


@step('Then I click on publish subscription')
def click_publish(step):
    time.sleep(2)
    publish  = world.browser.find_element_by_link_text('Open your Subscription for Member Enrollment')
    publish.click()


@step('Then subscription is published')
def subscription_published(step):
    time.sleep(2)
    check = world.browser.find_element_by_class_name('well')
    assert('This Subscribe is Open for Enrollment'  in  check.text)