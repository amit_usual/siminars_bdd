from selenium import webdriver
import lettuce_webdriver.webdriver
from selenium.webdriver.common.keys import Keys
from lettuce import *
import time
import sys
sys.path.append('/home/aayush/work/siminars_bdd')
import settings


@step('Given I am on siminars.com on bundle tab')
def goto_bundle(step):
    world.browser.get(settings.URl_STAGGING+"/profile")
    time.sleep(2)
    dropdown = world.browser.find_element_by_id("down")
    dropdown.click()
    time.sleep(1)
    manage_plan = world.browser.find_element_by_link_text('Manage Plans')
    manage_plan.click()
    time.sleep(4)
    new_plan = world.browser.find_element_by_link_text('Manage')
    new_plan.click()
    url_for_bundle = world.browser.current_url + '/bundles'
    world.browser.get(url_for_bundle)


@step('I click on Add a Bundle or Series')
def click_on_bundle(step):
    time.sleep(4)
    bundle = world.browser.find_element_by_class_name("js_add_bundle_link")
    bundle.click()


@step('Then add Bundle Title,choose Type and click create')
def add_bundle_name(step):
    time.sleep(4)
    title = world.browser.find_element_by_name("title")
    title.send_keys("My bundle")
    enforce_sequence = world.browser.find_element_by_name("enforce_sequence")
    enforce_sequence.click()
    enforce_sequence.send_keys(Keys.ARROW_DOWN)
    enforce_sequence.send_keys(Keys.ENTER)
    create_bundle = world.browser.find_element_by_link_text('Create')
    create_bundle.click()


@step('Then Bundle is created')
def bundle_create(step):
    time.sleep(4)
    assert ("47020" in world.browser.current_url)


@step('Given I am on siminars.com bundle summary page')
def goto_bundle_summary(step):
    assert("summary.sv" in world.browser.current_url)


@step('I click on Add a courses, selecte course and click add')
def add_bundle_content(step):
    time.sleep(2)
    content_list = world.browser.find_element_by_name("add_siminar")
    content_list.click()
    content_list.send_keys(Keys.ARROW_DOWN)
    content_list.send_keys(Keys.ENTER)
    add = world.browser.find_element_by_link_text('Add')
    add.click()


@step('Then course is added')
def content_added(step):
    content_list = world.browser.find_element_by_id("js_siminar_list")
    # added success test


@step('I click on Set Price & Publish')
def click_price(step):
    time.sleep(4)
    set_price = world.browser.find_element_by_class_name("smstepaddcontrol")
    set_price.click()
    time.sleep(4)


@step('Then set list price, purchase price and click on set bundle price')
def set_price(step):
    #set_list_price = world.browser.find_element_by_name("list_price")
    #set_list_price.send_keys('10')
    set_purchase_price = world.browser.find_element_by_name("purchase_price")
    set_purchase_price.send_keys('10')
    set_click_price = world.browser.find_element_by_id("js_set_price")
    set_click_price.click()


@step('Then bundle price set')
def price_set(step):
    #set_list_price = world.browser.find_element_by_name("list_price")
    set_purchase_price = world.browser.find_element_by_name("purchase_price")
    assert(set_purchase_price.get_attribute('value') == '10')


@step('I click on Coupons and Add a Coupon')
def click_coupons(step):
    price_url = world.browser.current_url
    Coupon_url = price_url.replace('price', 'coupons')
    world.browser.get(Coupon_url)
    time.sleep(2)


@step('Then set coupon code, price with coupon, expire date and click save')
def add_coupon(step):
    add = world.browser.find_element_by_link_text('Add a Coupon')
    add.click()
    time.sleep(2)
    code = world.browser.find_element_by_name("coupon")
    code.send_keys('aayush')
    price = world.browser.find_element_by_name("price")
    price.send_keys('4')
    save = world.browser.find_element_by_class_name("on_true")
    save.click()


@step('Then coupon is created')
def coupon_created(step):
    time.sleep(2)
    coupon_table = world.browser.find_element_by_id('js_ctable')
    assert('aayush' in coupon_table.text)


@step('Then done and next')
def done_next(step):
    time.sleep(2)
    done_next = world.browser.find_element_by_class_name("btn-success")
    done_next.click()


@step('Then add Topics')
def add_topic(step):
    pass
    #import pdb;pdb.set_trace()
    #topic= world.browser.find_element_by_xpath("//*[@placeholder='Enter Topic or Keyword']")
    # topic.send_keys('aa')
    # time.sleep(1)
    #selecte = world.browser.find_element_by_class_name("smtopicsearch")
    # selecte.send_keys(Keys.ARROW_DOWN)


@step('Then add Promotional Video, Promotional Description')
def add_video(step):
    time.sleep(2)
    summary_url = world.browser.current_url
    promotional_url = summary_url.replace('summary', 'promotion')
    world.browser.get(promotional_url)
    time.sleep(2)
    video = world.browser.find_element_by_name("val")
    video.send_keys("https://www.youtube.com/watch?v=TBt1f5XxOtw")
    next_btn = world.browser.find_element_by_class_name('next')
    next_btn.click()


@step('Then set Sign-up Page Layout')
def add_layout(step):
    time.sleep(2)
    promotional_url = world.browser.current_url
    layout_url = promotional_url.replace('promotion', 'layout')
    world.browser.get(layout_url)
    layout = world.browser.find_element_by_name("selectedlayout")
    layout.click()
    layout.send_keys(Keys.ARROW_DOWN)
    layout.send_keys(Keys.ENTER)
    apply_btn = world.browser.find_element_by_link_text('Apply')
    apply_btn.click()


@step('Given I am on siminars.com bundle launch page')
def lunch_page(step):
    time.sleep(2)
    layout_url = world.browser.current_url
    lunch_url = layout_url.replace('signup/layout', 'launch')
    world.browser.get(lunch_url)


@step('Then click on launch bundle')
def lunch_bundle(step):
    time.sleep(2)
    lunch_btn = world.browser.find_element_by_id("launch_bundle")
    lunch_btn.click()


@step('Then bundle lunched')
def lunched(step):
    time.sleep(2)
    bundle_lunched = world.browser.find_element_by_class_name('well')
    assert("This Bundle is Open for Enrollment" in bundle_lunched.text)
