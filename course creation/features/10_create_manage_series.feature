Feature: Manage plan on siminars
  As a siminars user
  I should be able to create series and manage

  Scenario: Create series
    Given I am on siminars.com on series tab
    I click on Add a Series
    Then add series Title,choose Type and click create
    Then series is created

  Scenario: Convert series into bundle
    Given I am on bundle summary page
    I click on options
    Then click on bundle type, selete bundle and save
    Then type changed

  Scenario: Delete bundle
    Given I am on bundle summary page
    I click on options
    Then click on Delete Bundle and Confirm Delete
    Then bundle deleted