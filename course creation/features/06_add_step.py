from selenium import webdriver
import lettuce_webdriver.webdriver
from lettuce import *
import time
import sys
sys.path.append('/home/aayush/work/siminars_bdd')
import settings


@step('I am on course summary page')
def on_summary_page(step):
    current_url = world.browser.current_url
    assert ("summary.sv" in current_url and '18098' in current_url)


@step('When I select add step')
def and_step(step):
    time.sleep(2)
    check = world.browser.find_element_by_class_name('smprogresscenter')
    if("Your Course is empty" in check.text):
        add_step = world.browser.find_element_by_class_name('smemptystepstart')
        add_step.click()
    else:
        add_step = world.browser.find_element_by_class_name('smaddstep') 
        add_step.click()

    time.sleep(2)
    title = world.browser.find_element_by_name('title')
    title.send_keys('test step')
    save = world.browser.find_element_by_class_name('on_true')
    save.click()


@step('Then I should be step create')
def step_create(step):
    time.sleep(4)
    check_text = world.browser.find_element_by_class_name('smemptystep')
    assert ('Start Adding Content' in check_text.text)


@step('click on set banner')
def click_banner(step):
    changebanner = world.browser.find_element_by_class_name('smchangebanner')
    changebanner.click()