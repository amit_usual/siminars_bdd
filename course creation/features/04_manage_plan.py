from selenium import webdriver
import lettuce_webdriver.webdriver
from selenium.webdriver.common.keys import Keys
from lettuce import *
import time
import sys
sys.path.append('/home/aayush/work/siminars_bdd')
import settings


@step('Given I am on siminars.com plan page')
def goto_plan(step):
    world.browser.get(settings.URl_STAGGING+"/profile")
    time.sleep(4)
    dropdown = world.browser.find_element_by_id("down")
    dropdown.click()
    manage_plan = world.browser.find_element_by_link_text('Manage Plans')
    manage_plan.click()
    time.sleep(4)
    new_plan = world.browser.find_element_by_link_text('Manage')
    new_plan.click()


@step('When I click on change plan nick name and set name')
def change_name(step):
    change_link = world.browser.find_element_by_id("edit_nickname_btn")
    change_link.click()
    set_name = world.browser.find_element_by_class_name("svfullwidth")
    set_name.clear()
    set_name.send_keys('test plan')
    save_name = world.browser.find_element_by_class_name("on_true")
    save_name.click()

@step('Then plan name setted')
def plan_name_setted(step):
    time.sleep(2)
    save_name =  name = world.browser.find_element_by_id('js-plan-name')
    assert('test plan' in save_name.text)

@step('When I click on billing account and select')
def select_account(step):
    select = world.browser.find_element_by_id("payment_account_select")
    select.click()
    select.send_keys(Keys.ARROW_DOWN)
    select.send_keys(Keys.ENTER)


@step('Then I click Update payment account')
def update_account(step):
    update = world.browser.find_element_by_id('set_payment_account_btn')
    update.click()


@step('Then payment account updated')
def payment_updated(step):
    select = world.browser.find_element_by_id("payment_account_select")
    assert('stripe' in select.text)

@step('When I click on payout account and select')
def select_payout_account(step):
    select = world.browser.find_element_by_id("payout_account_select")
    select.click()
    select.send_keys(Keys.ARROW_DOWN)
    select.send_keys(Keys.ENTER)


@step('Then I click Update payout account')
def update_payout_account(step):
    update = world.browser.find_element_by_id('set_payout_account_btn')
    update.click()

@step('Then payout account updated')
def payout_updated(step):
    select = world.browser.find_element_by_id("payout_account_select")
    assert('MyPayPal' in select.text)