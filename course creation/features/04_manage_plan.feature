Feature: Manage plan on siminars
  As a siminars user
  I should be able to manage plan

  Scenario: Change plan nick name
    Given I am on siminars.com plan page
    When I click on change plan nick name and set name
    Then plan name setted
 
  Scenario: Update payment 
    Given I am on siminars.com plan page
  	When I click on billing account and select
  	Then I click Update payment account
    Then payment account updated

  Scenario: Upadate payout method
    Given I am on siminars.com plan page
  	When I click on payout account and select
  	Then I click Update payout account
    Then payout account updated