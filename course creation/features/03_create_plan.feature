Feature: Create a plan on siminars
  As a siminars user
  I should be able to create a plan

  Scenario: Create plan
    Given I am on siminars.com
    When I click on profile dropdown and then manage plan and then new plan
    Then choose plan Retail or private
    Then choose your plan Starter, Basic, Pro, Expert
    Then plan is created
 