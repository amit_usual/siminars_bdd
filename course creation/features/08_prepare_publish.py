from selenium import webdriver
import lettuce_webdriver.webdriver
from selenium.webdriver.common.keys import Keys
from lettuce import *
import time
import sys
sys.path.append('/home/aayush/work/siminars_bdd')
import settings


@step('Given I am on siminars.com course')
def on_course(step):
    current_url = world.browser.current_url
    assert ("sv=e" in current_url and '18098' in current_url)


@step('When I click on Syllabus')
def click_syllabus(step):
    syllabus = world.browser.find_element_by_link_text("Syllabus")
    syllabus.click()


@step('Then click on Prepare to Publish')
def click_publish(step):
    time.sleep(2)
    prepare = world.browser.find_element_by_class_name("smoptunlaunched")
    prepare.click()


@step('Then choose plan')
def choose_plan(step):
    time.sleep(4)
    retail = world.browser.find_element_by_class_name("smwidebtn")
    retail.click()
    time.sleep(4)
    plan = world.browser.find_element_by_class_name("on_true")
    plan.click()


@step('Then plan is set')
def plan_set(step):
    time.sleep(2)
    plan = world.browser.find_element_by_class_name("smoptedition")
    assert ('Retail' in plan.text)


@step('Given I am on siminars.com course summary page')
def on_course_summary(step):
    current_url = world.browser.current_url
    assert ("sv=e" in current_url and '18098' in current_url)


@step('Then click on Publish')
def click_on_publish(step):
    time.sleep(2)
    prepare = world.browser.find_element_by_class_name("smoptunlaunched")
    prepare.click()
    time.sleep(2)


@step('Then goto publish tab and click on publish')
def goto_publish(step):
    prepare = world.browser.find_element_by_class_name("launch")
    prepare.click()
    time.sleep(2)
    launchnow = world.browser.find_element_by_class_name("launchnow")
    launchnow.click()
    time.sleep(2)
    agree = world.browser.find_element_by_class_name("on_true")
    agree.click()
    time.sleep(2)


@step('Then course is Publish')
def pulished(step):
    published = world.browser.find_element_by_class_name("launched_siminar")
    assert ('Share Your Siminar:' in published.text)

@step('I click on options button')
def click_options(step):
    time.sleep(2)
    options = world.browser.find_element_by_class_name("smoptadvanced")
    options.click()


@step('I click on copy couse')    
def click_copy(step):
    time.sleep(2)
    copy_siminar =  world.browser.find_element_by_id("copy_siminar")
    copy_siminar.click()
    time.sleep(2)
    on_true = world.browser.find_element_by_class_name("on_true")
    on_true.click()

@step('Then couser should be copyed')
def check(step):
    pass