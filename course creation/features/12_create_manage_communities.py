from selenium import webdriver
import lettuce_webdriver.webdriver
from selenium.webdriver.common.keys import Keys
from lettuce import *
import time
import sys
sys.path.append('/home/aayush/work/siminars_bdd')
import settings


@step('Given I am on Communities tab')
def goto_communities(step):
    world.browser.get(settings.URl_STAGGING+"/profile")
    time.sleep(2)
    dropdown = world.browser.find_element_by_id("down")
    dropdown.click()
    manage_plan = world.browser.find_element_by_link_text('Manage Plans')
    manage_plan.click()
    time.sleep(4)
    new_plan = world.browser.find_element_by_link_text('Manage')
    new_plan.click()
    time.sleep(2)
    url_for_communities = world.browser.current_url + '/communities'
    world.browser.get(url_for_communities)


@step('I click on Create communities tab')
def goto_create(step):
    time.sleep(2)
    create = world.browser.find_element_by_link_text('Create New')
    create.click()


@step('Then add Communities title, choose type and click Create')
def add_communit(step):
    time.sleep(4)
    communities_title = world.browser.find_element_by_name("title")
    communities_title.send_keys('test Communities')
    communities_type = world.browser.find_element_by_xpath(
        "//*[@name='plan_type'][@value='retail']")
    communities_type.click()
    create = world.browser.find_element_by_id('createCommunity')
    create.click()


@step('Then Communities is Created')
def communities_create(step):
    time.sleep(2)
    check = world.browser.find_element_by_class_name("table-striped")
    assert('test Communities' in check.text)


@step('I click on Communities assign')
def click_assign(step):
    time.sleep(2)
    assign = world.browser.find_element_by_link_text("Assign Courses")
    assign.click()
    time.sleep(2)


@step('I selecte communities and assign')
def set_communities(step):
    time.sleep(2)
    assign = world.browser.find_element_by_link_text("Assign")
    assign.click()
    time.sleep(2)
    communit = world.browser.find_element_by_class_name("form-control")
    communit.click()
    communit.send_keys(Keys.ARROW_DOWN)

    save = world.browser.find_element_by_class_name("on_true")
    save.click()


@step('Then Communities is assign')
def check(step):
    time.sleep(2)
    check = world.browser.find_element_by_class_name("table-striped")
    assert('test Communities' in check.text)
