Feature: Add contents a course to siminars
	As a siminars user
	I should be able to add different type content a course

	Scenario: Add action to-do course
   	  When I select action to-do on add lesson
    	And I add description 
    	Then action to-do should be create  

  	Scenario: Add audio to course
   	  When I select audio on add lesson
    	And I added audio link
    	Then audio should be added
    
    Scenario: Add blog to course
      When I select blog on add lesson
      And I added title, body and save
      Then blog is created
    
    Scenario: Add book to course
      when I slect Book Reading on add lesson
      And I search book and add it
      Then book read is created

    Scenario: Add slide to course
   	  When I select slide on add lesson
    	And I added slide link
    	Then slide should be added	

    Scenario: Add video to course
      I create other step
   	  When I select video on add lesson
    	And I added video link
    	Then video should be added

    Scenario: Add Discussion Question to course
      When I select Discussion Question on add lesson
      And I added Discussion Question and save it
      Then Discussion Question should be created 		

    Scenario: Add Poll to course
      When I select Poll on add lesson
      And I added Poll Question, Poll Response Options and save it
      Then Poll should be created  

    Scenario: Add quiz to-do course
      When I select quiz on add lesson
      I add title and description 
      I click save and next
      I Add question and Answer Options
      I click save and next
      I click save and next
      I click done
      Publish quiz
      Then quiz should be create  