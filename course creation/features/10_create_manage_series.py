from selenium import webdriver
import lettuce_webdriver.webdriver
from selenium.webdriver.common.keys import Keys
from lettuce import *
import time
import sys
sys.path.append('/home/aayush/work/siminars_bdd')
import settings


@step('Given I am on siminars.com on series tab')
def goto_series(step):
    world.browser.get(settings.URl_STAGGING+"/profile")
    time.sleep(2)
    dropdown = world.browser.find_element_by_id("down")
    dropdown.click()
    manage_plan = world.browser.find_element_by_link_text('Manage Plans')
    manage_plan.click()
    time.sleep(4)
    new_plan = world.browser.find_element_by_link_text('Manage')
    new_plan.click()
    time.sleep(4)
    url_for_bundle = world.browser.current_url + '/bundles'
    world.browser.get(url_for_bundle)


@step('I click on Add a Series')
def click_on_series(step):
    time.sleep(4)
    bundle = world.browser.find_element_by_class_name("js_add_bundle_link")
    bundle.click()


@step('Then add series Title,choose Type and click create')
def add_series_name(step):
    time.sleep(4)
    title = world.browser.find_element_by_name("title")
    title.send_keys("My series")
    enforce_sequence = world.browser.find_element_by_name("enforce_sequence")
    enforce_sequence.click()
    enforce_sequence.send_keys(Keys.ENTER)
    create_bundle = world.browser.find_element_by_link_text('Create')
    create_bundle.click()

@step('Then series is created')
def series_create(step):
    time.sleep(4)
    assert("47020" in world.browser.current_url)

@step('Given I am on bundle summary page')
def on_bundle_summary(step):
    assert("summary.sv" in world.browser.current_url)

@step('I click on options')
def click_options(step):
    options = world.browser.find_element_by_link_text('Options')
    options.click()



@step('Then click on bundle type, selete bundle and save')
def type_bundle(step):
    time.sleep(2)
    options_select = world.browser.find_element_by_name("plan_type")
    options_select.click()
    options_select.send_keys(Keys.ARROW_UP)
    save = world.browser.find_element_by_link_text("Save Changes")
    save.click()
    time.sleep(2)
    warning_btn = world.browser.find_element_by_link_text('Convert to Bundle')
    warning_btn.click()


@step('Then type changed')
def type_changed(step):
    time.sleep(2)
    check = world.browser.find_element_by_class_name('smsumoptions')
    assert(check.text.split()[0]=='Bundle')

@step('Given I am on bundle summary page')
def bundle_summary(step):
    world.browser.get(world.browser.current_url)
    time.sleep(2)
    assert("47020" in world.browser.current_url)

@step('I click on options')
def click_options(step):
    options = world.browser.find_element_by_link_text('Options')
    options.click()

@step('Then click on Delete Bundle and Confirm Delete')
def delete_bundle(step):
    time.sleep(2)
    delete = world.browser.find_element_by_id("js_del_bundle")
    delete.click()
    time.sleep(2)
    warning_btn = world.browser.find_element_by_link_text('Permanently Delete')
    warning_btn.click()

@step('Then bundle deleted')
def deleted(step):
    pass