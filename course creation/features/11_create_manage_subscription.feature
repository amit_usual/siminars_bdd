Feature: Manage plan on siminars
  As a siminars user
  I should be able to create Subscription and manage

  Scenario: Create Subscription
    Given I am on siminars.com plan page and on Subscription tab
    I click on Add a Membership Subscription
    Then add Membership Subscription Name and click Create
    Then Subscription is Created

 Scenario: Add month 
 	Given I am on siminars.com subscription summary page
 	I click on add monthly installment
 	Then monthly installment is added

 Scenario: Add content to month 
 	I click on add content
 	Then selecte course and click add
 	Then course is addded

 Scenario: Publish month
 	Given I am on subscription summary page
 	Then I click on publish month
 	Then month is published

 Scenario: Set price 
 	I click on price
 	Then set List Price, Monthly Fee 
 	Then click on Set Subscription Price
 	Then  Subscription price setted  

Scenario: Publish subscription
 	Given I am on subscription publish page
 	Then I click on publish subscription
 	Then subscription is published

