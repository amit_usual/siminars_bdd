from selenium import webdriver
import lettuce_webdriver.webdriver
from lettuce import *
import time
import sys
sys.path.append('/home/aayush/work/siminars_bdd')
import settings


@step('Given I am on siminars.com')
def on_siminars(step):
    world.browser.get(settings.URl_STAGGING+"/profile") 

@step('When I click on profile dropdown and then manage plan and then new plan')
def click_on_dropdown(step):
	time.sleep(4)
	dropdown = world.browser.find_element_by_id("down")
	dropdown.click()
	manage_plan = world.browser.find_element_by_link_text('Manage Plans')
	manage_plan.click()
	time.sleep(4)
	new_plan = world.browser.find_element_by_link_text('Create New Plan')
	new_plan.click()

@step('Then choose plan Retail')
def choose_plan(step):
	time.sleep(4)
	create_retail = world.browser.find_element_by_link_text('Create Retail Edition')
	create_retail.click()

@step('Then choose your plan Starter, Basic, Pro, Expert')
def choose_plan_price(step):
	time.sleep(4)
	expert = world.browser.find_element_by_xpath("//*[@name='tier_name'][@value='plan_retail_expert']")
	expert.click()
	get_plan = world.browser.find_element_by_link_text('Get this Plan')
	get_plan.click()

@step('Then plan is created')
def  plan_created(step):
	time.sleep(4)
	assert(settings.URl_STAGGING+"/plan" in world.browser.current_url)
