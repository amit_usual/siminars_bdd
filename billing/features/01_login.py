from selenium import webdriver
import lettuce_webdriver.webdriver
from lettuce import *
import time
import sys
sys.path.append('/home/aayush/work/siminars_bdd')
import settings

@before.all
def setup_browser():
    world.browser = webdriver.Firefox()


@step('I am on siminars.com/continue')
def on_siminars(step):
    world.browser.get(settings.URl_STAGGING+"/continue")


@step('I add email and password')
def add_email_and_password(step):
    email = world.browser.find_element_by_name("email")
    email.send_keys(settings.CONSUMER_NAME )
    password = world.browser.find_element_by_name("password")
    password.send_keys(settings.PASSWORD)
    signUp = world.browser.find_element_by_class_name("signupButton")
    signUp.click()


@step('I should be logged in')
def should_be_logged_in(step):
    time.sleep(4)
    assert (world.browser.current_url == settings.URl_STAGGING+"/profile")
