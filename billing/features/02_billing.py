from selenium import webdriver
import lettuce_webdriver.webdriver
from lettuce import *
import time
import sys
sys.path.append('/home/aayush/work/siminars_bdd')
import settings

@step('Given I am on the History tab in Billing.')
def history_tab(step):
    time.sleep(4)
    world.browser.get(settings.URl_STAGGING+"/billing")


@step('I click on "Refund" button if it is available')
def click_refund(step):
    refund = world.browser.find_element_by_class_name("table-striped")
    if "Refund" in refund.text:
        refund_btn = world.browser.find_element_by_link_text("Refund")
        refund_btn.click()
        time.sleep(4)


@step('I click on "Refund" button on the modal')
def conform_refund(step):
    conform = world.browser.find_element_by_class_name("on_true")
    conform.click()
    time.sleep(4)
    conform = world.browser.find_element_by_class_name("on_false")
    conform.click()


@step('Then the "Refund" button should be replaced with "Refund Initiated"')
def refund_initiated(step):
    refund = world.browser.find_element_by_class_name("table-striped")
    assert('Refund Initiated' in refund.text)


@step('Given I am on Payment Accounts tab in Billing')
def payment_tab(step):
    time.sleep(4)
    world.browser.get(settings.URl_STAGGING+"/billing?mode=payment")


@step('I click on Add Credit Card')
def add_credit_card(step):
    time.sleep(4)
    credit_card = world.browser.find_element_by_link_text("Credit Card")
    credit_card.click()


@step('I fill in the dummy card details with a nickname')
def fill_from(step):
    name = world.browser.find_element_by_name("fullname")
    name.send_keys("aayush")
    
    amex = world.browser.find_element_by_xpath("//*[@value='amex']")
    amex.click()

    card_number = world.browser.find_element_by_name("card_number")
    card_number.send_keys("345137375174123")

    card_code = world.browser.find_element_by_name("card_code")
    card_code.send_keys("4242")

    expiration_date = world.browser.find_element_by_name("expiration_date")
    expiration_date.send_keys("12/18")

    billing_address_one = world.browser.find_element_by_name(
        "billing_address_one")
    billing_address_one.send_keys("113")

    billing_address_two = world.browser.find_element_by_name(
        "billing_address_two")
    billing_address_two.send_keys("113stp")

    billing_city = world.browser.find_element_by_name("billing_city")
    billing_city.send_keys("11")

    billing_state = world.browser.find_element_by_name("billing_state")
    billing_state.send_keys("11")

    billing_pincode = world.browser.find_element_by_name("billing_pincode")
    billing_pincode.send_keys("11")

    billing_country = world.browser.find_element_by_name("billing_country")
    billing_country.send_keys("11")

    nickname = world.browser.find_element_by_name("nickname")
    nickname.send_keys("11_test")


@step('I click on Add to add Card')
def click_add(step):
    save_account = world.browser.find_element_by_class_name("save_account")
    save_account.click()


@step('Then a new field with nickname matching to the entry appears on Payments Accounts table')
def check_add_card(step):
    time.sleep(6)
    check = world.browser.find_element_by_class_name("payment_table")
    assert('11_test' in check.text)

@step('Given I am on the Payout Methods tab in Billing')
def on_payout_methods(step):
    time.sleep(4)
    world.browser.get(settings.URl_STAGGING+"/billing?mode=payout")

@step('I click on Check option in Payout Method')
def click_check(step):
    check_btn = world.browser.find_element_by_link_text("Check")
    check_btn.click()
    

@step('I fill in the dummy check details with a nickname')
def dummy_check(step):
    payee_name = world.browser.find_element_by_name("payee_name")
    payee_name.send_keys("aayush")

    address_one = world.browser.find_element_by_name("address_one")
    address_one.send_keys("4242424242424242")

    city = world.browser.find_element_by_name("city")
    city.send_keys("pune")

    state = world.browser.find_element_by_name("state")
    state.send_keys("maha")

    address_two = world.browser.find_element_by_name("address_two")
    address_two.send_keys("113")

    zip_code = world.browser.find_element_by_name("zip_code")
    zip_code.send_keys("113121313")

    country = world.browser.find_element_by_name("country")
    country.send_keys("india")

    nickname = world.browser.find_element_by_name("nickname")
    nickname.clear()
    nickname.send_keys("Mycheck")


@step('I click on Save to save details')
def check_save(step):
    save_account = world.browser.find_element_by_class_name("save_account")
    save_account.click()

@step('Then a new field of Check nickname matching to the entry appears on the Payout Table')
def check(step):
    time.sleep(4)
    check = world.browser.find_element_by_class_name('all_accounts')
    assert('Mycheck' in check.text)

@step('I click on EFT option in Payout Method')
def click_check(step):
    check_btn = world.browser.find_element_by_link_text("EFT")
    check_btn.click()

@step('I fill in the dummy EFT details with a nickname')
def eft_details(step):
    payee_name = world.browser.find_element_by_name("payee_name")
    payee_name.send_keys("aayush")

    bank_name = world.browser.find_element_by_name("bank_name")
    bank_name.send_keys("SBI")

    routing_number = world.browser.find_element_by_name("routing_number")
    routing_number.send_keys("12184712847183")

    account_number = world.browser.find_element_by_name("account_number")
    account_number.send_keys("12184712847183")

    nickname = world.browser.find_element_by_name("nickname")
    nickname.clear()
    nickname.send_keys("MyEFT")

@step('Then a new field of EFT with nickname matching to the entry appears on the Payout Table')
def check(step):
    time.sleep(4)
    check = world.browser.find_element_by_class_name('all_accounts')
    assert('MyEFT' in check.text)  

@step('I click on PayPal option in Payout Method')
def click_check(step):
    check_btn = world.browser.find_element_by_link_text("Paypal")
    check_btn.click()    

@step('I fill in the dummy PayPal details with a nickname')
def paypal_details(step):
    payee_name = world.browser.find_element_by_name("payee_name")
    payee_name.send_keys("aayush")

    lastname = world.browser.find_element_by_name("lastname")
    lastname.send_keys("tiwari")

    email = world.browser.find_element_by_name("email")
    email.send_keys("xyz@gmail.com3")

    re_email = world.browser.find_element_by_name("re_email")
    re_email.send_keys("xyz@gmail.com3")

@step('Then a new field of PayPal with nickname matching to the entry appears on the Payout Table')
def check(step):
    time.sleep(4)
    check = world.browser.find_element_by_class_name('all_accounts')
    assert('MyPayPal' in check.text)