Feature: billing  to siminars
  As a siminars user
  I should be able to manage billing

  

  Scenario: Add Credit Card
  	Given I am on Payment Accounts tab in Billing
  	I click on Add Credit Card
  	I fill in the dummy card details with a nickname
  	I click on Add to add Card
  	Then a new field with nickname matching to the entry appears on Payments Accounts table


  Scenario: Add Check in Payout	
  	Given I am on the Payout Methods tab in Billing
  	I click on Check option in Payout Method
  	I fill in the dummy check details with a nickname
  	I click on Save to save details
  	Then a new field of Check nickname matching to the entry appears on the Payout Table

  Scenario: Add EFT in Payout
    Given I am on the Payout Methods tab in Billing
    I click on EFT option in Payout Method
    I fill in the dummy EFT details with a nickname
    I click on Save to save details
    Then a new field of EFT with nickname matching to the entry appears on the Payout Table

  Scenario: Add PayPal in Payout
    Given I am on the Payout Methods tab in Billing
    I click on PayPal option in Payout Method   
    I fill in the dummy PayPal details with a nickname
    I click on Save to save details
    Then a new field of PayPal with nickname matching to the entry appears on the Payout Table
