from selenium import webdriver
import lettuce_webdriver.webdriver
from lettuce import *
import time
import sys
sys.path.append('/home/aayush/work/siminars_bdd')
import settings


@step('Given I am on couser summary page')
def summary_page(step):
	 world.browser.get(
	     settings.URl_STAGGING+"/"+settings.SIMINARS_CONSUME+"/summary.sv")


@step('I click on test Action')
def click_on(step):
    time.sleep(3)
    action = world.browser.find_element_by_link_text("test Action")
    action.click()

@step('Then I able to read Action')
def check(step):
    time.sleep(3)
    check = world.browser.find_element_by_class_name("svpostheader")
    assert('test Action' in check.text)


@step('I click on next')
def click_on_next(step):
    time.sleep(3)
    action = world.browser.find_element_by_link_text("Next")
    action.click()


@step('Then I able to play audio')
def check(step):
    time.sleep(3)
    check = world.browser.find_element_by_class_name("svpostheader")
    assert('Na Jaane Kya Hai' in check.text)


@step('Then I able to read blog')
def check(step):
    time.sleep(3)
    check = world.browser.find_element_by_class_name("svpostheader")
    assert('test blog' in check.text)    



@step('Then I able to read book')
def check(step):
    time.sleep(3)
    check = world.browser.find_element_by_class_name("svpostheader")
    assert('Technical Manual' in check.text)       


@step('Then I able to read slide')
def check(step):
    time.sleep(3)
    check = world.browser.find_element_by_class_name("svpostheader")
    assert('Introduction to Python' in check.text)       


@step('Then I able to play video')
def check(step):
    time.sleep(3)
    check = world.browser.find_element_by_class_name("svpostheader")
    assert('Namak Haraam' in check.text)       

@step('I click on next step')
def next_step(step):
    time.sleep(3)
    action = world.browser.find_element_by_link_text("Next Step")
    action.click() 

@step('I click on take quiz')
def take_quiz(step):
    time.sleep(3)
    take = world.browser.find_element_by_id("js-unskip")
    take.click()

@step('I click on skips quiz')
def skips(step):
    time.sleep(3)
    skip = world.browser.find_element_by_class_name("js_skip")
    skip.click()
    time.sleep(3)
    skip_quiz = world.browser.find_element_by_link_text("Skip Quiz")
    skip_quiz.click()


@step("Then quiz is skipped")
def check(step):
    time.sleep(3)
    quiz = world.browser.find_element_by_class_name("smresultsummary")
    assert('You Skipped this Quiz' in quiz.text)

@step('Add Question to blog')
def add_question(step):
    time.sleep(3)
    editor4 = world.browser.find_element_by_class_name('sveditblob')
    editor4.click()
    editor4.send_keys("test comment")


@step('I click submit') 
def submit(step):
    submit = world.browser.find_element_by_link_text("Submit")
    submit.click()

@step('Then commit is posted') 
def check(step):
    check = world.browser.find_element_by_class_name("svresponsecontents")
    assert('test comment' in check.text)


@step('I click on response')  
def  click_response(step):
    respond = world.browser.find_element_by_link_text("Respond")
    respond.click()

@step('I add response')
def add_response(step):
    time.sleep(2)
    editor8 = world.browser.find_element_by_id("editor8")
    editor8.click()
    editor8.send_keys('test response')
    post_response = world.browser.find_element_by_link_text("Post Response")
    post_response.click()

@step('Then response is added')
def check(step):
    check = world.browser.find_element_by_class_name("svresponsecontents")
    assert('test response' in check.text)  
