from selenium import webdriver
import lettuce_webdriver.webdriver
from lettuce import *
import time
import sys
sys.path.append('/home/aayush/work/siminars_bdd')
import settings


@step('Given I am on course landing page')
def landging_page(step):
    world.browser.get(
        settings.URl_STAGGING+"/"+settings.SIMINARS+"/summary/signup.sv")


@step('I click on Buy Now')
def buy_now(step):
    time.sleep(2)
    buy_now = world.browser.find_element_by_link_text("Buy Now")
    buy_now.click()

@step('I click on next')
def click_next(step):
    time.sleep(2)
    click_next = world.browser.find_element_by_class_name("next")
    click_next.click()

@step('I click on pay now')
def pay_now(step):
    time.sleep(2)
    pay_now = world.browser.find_element_by_class_name("on_true")
    pay_now.click()

@step('Then i will able to access course')
def check(step):
    time.sleep(5)
    check = world.browser.find_element_by_class_name("modal-button-text")
    assert('Get Started' in check.text)

@step('Given I am on the History tab in Billing')
def history_tab(step):
    world.browser.get(settings.URl_STAGGING+"/billing")


@step('I click on "Refund" button if it is available')
def click_refund(step):
    refund = world.browser.find_element_by_class_name("table-striped")
    if "Refund" in refund.text:
        refund_btn = world.browser.find_element_by_link_text("Refund")
        refund_btn.click()
        time.sleep(4)


@step('I click on "Refund" button on the modal')
def conform_refund(step):
    conform = world.browser.find_element_by_class_name("on_true")
    conform.click()
    time.sleep(4)
    conform = world.browser.find_element_by_class_name("on_false")
    conform.click()


@step('Then the "Refund" button should be replaced with "Refund Initiated"')
def refund_initiated(step):
    refund = world.browser.find_element_by_class_name("table-striped")
    assert('Refund Initiated' in refund.text)    



@step('Given I am on bundle landing page')
def bundle_landing(step):
    world.browser.get(
        settings.URl_STAGGING+"/"+settings.SIMINARS+"/summary/signup.sv")


@step('Then the "Refund" button should be replaced with "Refund Initiated')
def check(step):
    pass    

@step('I select PayPal')
def select_paypal(step):
    paypal = world.browser.find_element_by_xpath("//*[@value='paypal']")
    paypal.click()
    

@step('I enter paypal account details')
def paypal(step):  
    time.sleep(4)

    email = world.browser.find_element_by_id("login_email")
    email.send_keys("mohandutt134-buyer@gmail.com")

    login_password = world.browser.find_element_by_id("login_password")
    login_password.send_keys("paypal123")
    submitLogin = world.browser.find_element_by_id("submitLogin")
    submitLogin.click()
    time.sleep(10)
    continue_buy = world.browser.find_element_by_id("continue")
    continue_buy.click()