Feature: Buy a course from siminars
  As a siminars user
  I should able to buy course

  Scenario: Buy a course with old card
    Given I am on course landing page
    I click on Buy Now
    I click on next
    I click on pay now
    Then i will able to access course


  Scenario: refund course
  	Given I am on the History tab in Billing
  	I click on "Refund" button if it is available   
  	I click on "Refund" button on the modal
  	Then the "Refund" button should be replaced with "Refund Initiated"


  Scenario: Buy a bundle with old card
    Given I am on bundle landing page
    I click on Buy Now
    I click on next
    I click on pay now
    Then i will able to access course

  Scenario: refund course
  	Given I am on the History tab in Billing
  	I click on "Refund" button if it is available   
  	I click on "Refund" button on the modal
  	Then the "Refund" button should be replaced with "Refund Initiated

  Scenario: Buy a course with old paypal account
    Given I am on course landing page
    I click on Buy Now
    I select PayPal
    I click on next
    I enter paypal account details
    Then i will able to access course


  Scenario: refund course
    Given I am on the History tab in Billing
    I click on "Refund" button if it is available   
    I click on "Refund" button on the modal
    Then the "Refund" button should be replaced with "Refund Initiated  